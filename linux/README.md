# Linux

- [rappels](./rappels-linux.md)

## La conteneurisation

- Les containers [cours](./containers.md) | [slides](./slides/containers.html) | [TP](./tp-containers/README.md)

## Advanced GNU/linux

- systemd : [cours](./systemd.md) | [slides](./slides/systemd.html) | [TP](./tp-systemd/README.md)
- Linux Hardening : [cours](./linux_os_hardening.md) | [TP](./tp-hardening/README.md)
- Tracing & Observability : [TP](./tp-tracing/README.md)
- [configuration kernel](./config-linux.md)
  - [Compilation pilote de périphérique](./tp-config-linux/compile-driver/README.md)
  - [Configuration ip avancée](./tp-config-linux/config-net/config-net-advanced.md)
  - [Configuration ip avancée avec networkd](./tp-config-linux/config-net/systemd-net.md)
  - [Configuration stockage iscsi multipath](./tp-config-linux/config-stockage/iscsi-mutlipath.md)
  - [Rappels LVM](./tp-config-linux/config-stockage/tp-lvm.md)
  - [Les scheduleur d'i/o](./tp-config-linux/config-stockage/scheduleur-io.md)
