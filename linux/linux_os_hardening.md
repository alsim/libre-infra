# Linux OS Hardening

On va aborder dans ce cours plusieurs techniques permettant d'augmenter le niveau de sécurité d'une machines GNU/Linux.

![Fully Harden](../images/full_harden.jpg)

# Sommaire

- [Linux OS Hardening](#linux-os-hardening)
- [Sommaire](#sommaire)
- [I. General good practices](#i-general-good-practices)
- [II. Syscalls](#ii-syscalls)
  - [1. C koa un syscall](#1-c-koa-un-syscall)
  - [2. Monitoring des syscalls](#2-monitoring-des-syscalls)
  - [3. Blocage de syscalls](#3-blocage-de-syscalls)
    - [A. seccomp](#a-seccomp)
    - [B. auditd](#b-auditd)
- [III. Intégrité et conformité](#iii-intégrité-et-conformité)
  - [1. Contrôle d'intégrité](#1-contrôle-dintégrité)
  - [2. Contrôle de conformité](#2-contrôle-de-conformité)
- [IV. Mandatory Access Control: SELinux](#iv-mandatory-access-control-selinux)
  - [1. Mandatory Access Control](#1-mandatory-access-control)
  - [2. Exemples concrets](#2-exemples-concrets)
  - [3. SELinux main config](#3-selinux-main-config)
  - [3. Labels](#3-labels)
  - [5. Configurer SELinux](#5-configurer-selinux)

# I. General good practices

On va pas s'étendre ici, mais on va citer quelques exemples :

- maîtrise de la stack réseau (firewall restrictif, maîtrise des services réseau)
- utilisation des permissions UNIX de façon restrictives
- partitionnement (paritions séparées, options de montage)
- utilisation d'outils de sécurité élémentaires (rsyslog, journald, fail2ban)
- mise à jour régulière et désinstallation des programmes obsolètes/inutilisés
- utilisation de mécanismes noyaux pour isoler les processus (chroot, namespaces, cgroups, capabilities)
- utiliser des outils automatisés pour gérer la configuration du système (Ansible, Puppet, etc.)
- centraliser les logs
- monitoring + alerting
- backup
- outils de sécurité spécifiques aux applications (WAF)

Il existe de très bon guides de références sur Internet, par exemple [les guides de CIS](https://learn.cisecurity.org/benchmarks).

# II. Syscalls

## 1. C koa un syscall

Les *syscalls* sont des fonctions du noyau GNU/Linux. Ces fonctions peuvent être appelées par des programmes qui évoluent dans le *user-space*.

Les *syscalls* sont utilisés par les programmes pour demander au kernel l'accès à certaines ressources, ou l'exécution de certaines tâches.

> Peu importe le langage utilisé, un programme passera des *syscalls* lors de son exécution.

Exemples de *syscalls* très souvent utilisés :

- `fork()` : permet de créer un nouveau processus
  - cet appel système est donc appelé à chaque fois que l'on lance un programme
- `open()` : ouvre un fichier
  - le fichier peut être ouvert en mode *read-only*, *write-only*, *read/write*
- `close()` : ferme un fichier précédemment ouvert
- `socket()` : crée un socket
  - aussi bien les sockets réseau (TCP et UDP sur des interfaces)
  - que des sockets UNIX
- `connect()` : initie une connexion depuis un socket
- `kill()` : envoie un signal à un processus
- `chmod()` : change les permissions liés à un fichier
- `unshare()` : crée un namespaces

> Certains outils élémentaires comme `strace` permettent de tracer les *syscalls* effectués par un programme donné, pendant son exécution. Parfois utile pour débugger certaines erreurs !

## 2. Monitoring des syscalls

**Les *syscalls* sont donc les fonctions que les programmes appellent pour effectuer des opérations liés à l'OS.**

Il est intéressant de monitorer les *syscalls* passés par les programmes en temps réel afin de surveiller leur comportement.

Certains comportements peuvent être rapidement détectés comme suspects, par exemple :

- utilisation de `open()` pour lire un fichier dans `/etc/` comme `/etc/passwd`
- utilisation de `fork()` de façon inopinée, comme pour lancer un shell `/bin/bash`
- utilisation de `unshare()` dans un environnement confiné, comme pour essayer d'escape un environnement conteneurisé
- utilisation de `connect()` par un programme qui n'est pas censé créer des connexions, mais uniquement en recevoir

**Surveiller les *syscalls* passés par les applications permet donc de détecter bon nombre de comportements suspects, et d'avoir une idée précise de la façon dont les programmes interagissent avec l'OS** (et par extension : avec le réseau, le filesystem, le noyau, etc.).

## 3. Blocage de syscalls

Plusieurs technologies utilisent la surveillance des *syscalls* afin d'apporter un meilleur niveau de sécurité à l'OS.

### A. seccomp

*seccomp* (pour *secure computing mode*) est un outil qui permet de définir une liste des appels système que pourra passer un programme pendant son exécution.

Peu flexible, on lui préfère souvent *seccomp-bpf*. *seccomp-bpf* est utilisé par OpenSSH, certains navigateurs web, Docker, qemu, et d'autres pour renforcer la sécurité du système lors de leur utilisation.

### B. auditd

*auditd* est un démon qui scrute les *syscalls* à la recherche de comportements anormaux. Plus haut niveau, il permet d'écrire des règles fines de manière simplifiée.

Il est bien intégré aux systèmes GNU/Linux modernes, par exemple, les logs remontent nativement dans `journalctl`.

> Il est présent par défaut sur la plupart des systèmes dérivés de RedHat.

Il peut repérer facilement les comportements anormaux comme l'ouverture de fichier dont l'accès doit être protégé, comme les fichiers contenus dans `/etc`.

> 📜 *Il est intéressant de remonter les logs de cet outil dans une gestion de logs centralisée, ainsi que configurer un système d'alerting.*

# III. Intégrité et conformité

## 1. Contrôle d'intégrité

Le contrôle d'intégrité désigne le fait de s'assurer de l'intégrité des fichiers du filesystem.

On qualifie aussi ces outils d'HIDS pour *Host-based Intrusion Detection System*. Le fonctionnement est simple :

- au premier lancement de l'outil
  - il établit une base de données des fichiers du filesystem
  - il stocke le hash de chaque fichier
- aux lancements suivants
  - il vérifie que les fichiers n'ont pas bougé, *via* la vérification du hash
- il génère des logs si un comportement anormal est détecté

On peut citer [AIDE](https://aide.github.io/) comme outil réputé.

> 📜 *Il est intéressant de remonter les logs de cet outil dans une gestion de logs centralisée, ainsi que configurer un système d'alerting.*

## 2. Contrôle de conformité

Le contrôle de conformité permet de vérifier l'état de la configuration d'un système.

Ici, on ne parle pas de l'intégrité des fichiers, ou de déployer une configuration spécifique, mais de vérifier qu'un système est conforme à une politique donnée.

Le contrôle de conformité va vérifier, selon des bonnes pratiques définies à l'avance, que le système est conforme à l'état voulu.

Un outil de référence qui peut être cité est [OpenSCAP](https://www.open-scap.org/).

> 📜 *Il est intéressant de remonter les logs de cet outil dans une gestion de logs centralisée, ainsi que configurer un système d'alerting.*

# IV. Mandatory Access Control: SELinux

SELinux est un outil initialement développé par la NSA dont le développement est aujourd'hui mené par Red Hat.  
**Ce produit est considéré comme très mature** : il est open-source depuis l'année 2000.

**SELinux permet d'améliorer drastisquement le niveau de sécurité d'une machine avec un OS GNU/Linux**, en apportant des concepts supplémentaires, s'ajoutant au simple contrôle d'accès traditionnel des systèmes UNIX (c'est à dire : des utilisateurs, des groupes, et des permissions `rwx` sur les fichiers).

> Il est présent par défaut sur RHEL et ses dérivés (CentOS, Rocky, Fedora, etc.). Il existe un équivalent présent par défaut sur Ubuntu : c'est AppArmor.

L'utilisation de SELinux apporte plusieurs bénéfices :

- contrôle d'accès beaucoup plus fin et granulaire qu'avec la gestion de permissions traditionnelle
- diminue drastiquement la possibilité d'exploiter une application vulnérable
- assure un très haut niveau de confidentialité des données
- isole les processus et les données dans des environnements confinés

## 1. Mandatory Access Control

On dit que les permissions UNIX sont un mécanisme DAC (*Discretionary Access Control*).  
C'est à dire que la gestion des droits et permissions est basée une notion de propriété des entités, des fichiers par exemple.  

> En effet, dans les OS GNU/Linux, être le propriétaire d'un fichier nous donne tous les droits sur ce fichier, et notamment, de transférer la propriété du fichier à un autre utilisateur du système.

En revanche, on dit que SELinux est un mécanisme MAC (*Mandatory Access Control*). Ce mécanisme ne remplace pas les permissions UNIX, mais s'y ajoute.  
Un mécanisme MAC ne se base pas sur une notion de propriété, mais simplement sur une configuration qui autorise ou non deux entités à interagir.

---

**SELinux est un système de whitelist** : tous les comportements qui ne sont pas explicitement autorisés seront considérés des erreurs.

> Par exemple : est-ce que le binaire `httpd` a le droit d'écouter sur le port `80/tcp` ? Si ce n'est pas explicitement configuré dans SELinux, alors une erreur `permission denied` apparaîtra.

---

**SELinux, c'est le feu. Le désactiver, c'est dire non à une sécurité forte.**

Fortement intégré aujourd'hui aux systèmes RedHat, la plupart des paquets qui viennent des dépôts RedHat sont accompagnés d'une politique SELinux, c'est implémenté par défaut.

Aussi, **systemd** sait interagir avec SELinux, c'est là encore natif.

## 2. Exemples concrets

Quelques exemples concrets de ce qu'on peut faire avec SELinux, je prends volontairement des cas un peu extrêmes, pour bien cerner le truc :

- enlever tous ses superpouvoirs à `root`
- `chmod 777` tout le disque, avec très peu de conséquences
- empêcher un programme de faire quoique ce soit d'autre que écouter sur un port donné et écrire dans un fichier spécifique
- confiner des utilisateurs dans un environnement restreint

![Permission denied](../images/permission_denied.jpg)

## 3. SELinux main config

Le fichier de conf principal de SELinux se trouve dans `/etc/selinux/config`.

```bash
$ cat /etc/selinux/config
SELINUX=enforcing
SELINUXTYPE=targeted
```

➜ `SELINUX=` peut prendre 3 valeurs :

- `disabled` : SELinux est complètement désactivé
- `permissive` : SELinux log tous les évènements qui ne sont pas explicitement autorisés, mais ne bloque rien
- `enforcing` : SELinux log tous les évènements qui ne sont pas explicitement autorisés, et bloque ces évènements

Le mode `permissive` est très utile pour la configuration de SELinux (on verra ça plus bas).
Une fois la conf en place, le mode `enforcing` doit être utilisé.

On peut utiliser les commandes `sestatus` ou `getenforce` pour avoir des informations sur l'état actuel de SELinux.

La commande `setenforce` permet de basculer du mode `permissive` au mode `enforcing`, ou inversement.

➜ `SELINUXTYPE=` définit le type de la politique SELinux appliquée sur le système. On peut nous même en créer, mais par défaut, on peut lui faire prendre les valeurs :

- `targeted`
  - politique par défaut
  - confine les entités (processus, utilisateurs, fichiers, etc.) que l'on a configuré
  - il existe un espace non-confiné ou aucune restriction n'est appliquée par défaut
- `mls`
  - politique plus forte
  - il n'existe pas d'espace non-confiné
  - les entités sont placés dans un niveau de confidentialité (modèle de Bell-La Padula: "no read up, no write down")

Le commun des mortels utilise souvent le type `targeted`, c'est ce que nous allons voir dans ce cours.

## 3. Labels

Les *labels* sont au coeur du fonctionnement de SELinux. Un label est apposé sur toutes les entités que manipulent les SELinux : utilisateurs , fichiers, processus, ports, etc.

Pour visusaliser les labels, beaucoup de commandes courantes supportent l'option `-Z` :

```bash
# Lister des labels apposés sur des fichiers
$ ls -al -Z

# Lister des labels apposés sur les processus
$ ps -ef -Z

# Afficher le label de l'utilisateur courant
$ id -Z

# Afficher les labels des ports en écoute en cours d'utilisation
$ sudo ss -lnptuZ
```

> Ce ne sont là que des exemples, beaucoup d'autres commandes supportent un `-Z` pour manipuler les labels SELinux.

Ce *label* est appelé le *contexte SELinux* de l'entité. Il est sous la forme :

```selinux_label
$ ls -alZ
-rwxrwxr-x. trogdor trogdor unconfined_u:object_r:user_home_t:s0 testfile```
```

- `unconfined_u` est l'utilisateur qui peut gérer cette ressource
- `object_r` est le rôle de cette ressource
- `user_home_t` est le type de la ressource
- `s0` est le niveau de confidentalité de la ressource

> Dans un système configuré avec le type de politique `targeted`, on ne gère jamais le niveau de confidentialité.

SELinux se sert de ces contextes pour savoir si telle entité peut interagir avec telle autre.

Par exemple, SELinux peut autoriser le processus `httpd` à lire des fichiers avec un certain contexte, mais il ne pourra en lire aucun autre

> A noter que ce genre de comportements basiques sont déjà préconfigurés dans les installations modernes de SELinux.

## 5. Configurer SELinux

Avant c'était un peu une plaie, maintenant on a des tools cools pour gérer tout ça. Ca sera + exploré pendant le TP, mais les outils principaux vont être :

- `semanage` permet d'interagir avec SELinux et faire plein de bailz
- `chcon` permet de changer le contexte d'un fichier
- `restorecon` permet de restaurer le contexte par défaut d'un fichier
- `audit2allow` permet de générer des règles SELinux à partir des logs
- `journalctl` permet de visualiser clairement les dernières infractions aux règles SELinux (avec `setroubleshoot`)

![Hack Linux](../images/hack_linux.jpg)