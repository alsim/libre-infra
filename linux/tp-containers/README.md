# TP Containers

Plusieurs TPs à la carte, afin de s'adapter au mieux aux différentes envies et profils.

Je vous recommande d'utiliser Rocky Linux pour dérouler les TPs, vous pouvez sinon utiliser la distrib de votre choix.

- [Linux containers in-depth](./ct-in-depth/README.md)
- [Containers tooling : Skopeo, Buildah, Falco, Trivy](./ct-tooling/README.md)
- [Be the fastest](./be-the-fastest/README.md)