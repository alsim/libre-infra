# Be the fastest

Le but de ce mini-TP est un concours entre nous (ouais je participe aussi) pour construire le conteneur le plus "opti" possible. Les critères seront les suivants :

- taille de l'image
- temps d'exécution

Mais aussi :

- temps de build
- espace RAM occupé

La taille de l'image, ça se check facilement. Le reste, on le calculera avec un `/usr/bin/time -v [COMMAND]`.

Les consignes :

- le programme doit **compter** de 1 à 100000, et **afficher** chacun des entiers
- vous pouvez vous servir de n'importe quelle image de base
- vous pouvez vous servir de n'importe quel runtime/outil de conteneurisation
- vous pouvez utiliser n'importe quel langage
- on utilise les standards !
  - l'image utilisée doit être compatible OCI
  - le runtime utilisé doit être compatible OCI

![TOO BIG](../../../images/too_big.jpg)
