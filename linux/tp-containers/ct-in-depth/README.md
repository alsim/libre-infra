# Linux Containers in-depth

**Le but du TP est d'explorer ce qu'il y a sous le capot lorsqu'on lance des conteneurs.**

La conteneurisation désigne notamment le fait d'exécuter un ou plusieurs processus dans un environnement isolé, isolé par rapport au système hôte.

C'est donc notamment avec un point de vue orienté sécurité que nous allons effectuer notre spéléologie conteneurisée.

![Just another process](../../../images/just_another_process_2.png)

- [Linux Containers in-depth](#linux-containers-in-depth)
- [0. Prerequisites](#0-prerequisites)
- [I. Docky Docky Docker](#i-docky-docky-docker)
  - [1. Setup](#1-setup)
  - [2. Docker, who are you ?](#2-docker-who-are-you-)
  - [3. Isolation you said ?](#3-isolation-you-said-)
  - [4. Capabilities](#4-capabilities)
- [II. Podman](#ii-podman)

# 0. Prerequisites

Un OS Linux récent et à jour. Vous pouvez utiliser votre hôte comme une VM.

Plutôt une VM parce qu'on va installer/désinstaller des trucs qui posent des conflits entre eux, donc bon... c:

# I. Docky Docky Docker

![Please tell me more](../../../images/please_tell_me_more.jpeg)

## 1. Setup

➜ Installer Docker sur la machine

- pour l'install, référez-vous à la [doc officielle](https://docs.docker.com/engine/install/)

```bash
# une fois installé, démarrez-le service Docker
$ sudo systemctl start docker
$ sudo systemctl status docker

# ajoutez votre utilisateur au groupe `docker`, afin de pouvoir utiliser les commandes `docker` sans avoir besoin de l'identité de `root`
$ sudo usermod -aG docker $(whoami)
# il est nécessaire de lancer un nouveau terminal pour que ce changement prenne effet

# vérifiez votre install
$ docker info
$ docker run fedora /usr/bin/true
$ docker ps -a
```

---

Il faut être `root` ou membre du groupe `docker` pour utiliser le CLI `docker`. La raison est simple : les commandes `docker` écrive dans le socket UNIX `/var/run/docker.sock` :

```bash
$ ls -al /var/run/docker.sock
srw-rw----. 1 root docker 0 Mar  9 17:12 /var/run/docker.sock
```

## 2. Docker, who are you ?

On va inspecter un peu les processus lancés par Docker pendant son fonctionnement.

**Prenez bien note de quel utilisateur lance chacun de ces processus.**

➜ **Processus du service Docker**

```bash
# assurez-vous qu'aucun conteneur ne tourne, pour n'avoir que les processus liés au démon Docker
$ docker ps

# suppression de tous les conteneurs actifs
$ docker rm -f $(docker ps -aq)

# déterminez les processus liés à l'exécution du service docker
$ ps -ef
$ systemctl status docker
```

➜ **Exécution d'un conteneur**

```bash
# assurez-vous qu'aucun conteneur ne tourne
$ docker ps
$ docker rm -f $(docker ps -aq)

# lancez un conteneur de test, qui sera facilement identifiable, par exemple
$ docker run -d fedora sleep 999999 # un bon vieux sleep avec plein de 99999, c'est facilement identifiable :)

# repérez les processus liés à l'exécution de ce conteneur
$ ps -ef
```

---

➜ **Il y a plusieurs processus qui vous ont peut être échappé, car leur durée de vie est très courte.** Ils existent pour initialiser l'environnement du conteneur. Il est possible de les repérer de plein de façons différentes.

Je vous oblige pas à le faire, ça peut être pénible, voici les process qu'on peut trouver :

```bash
root       17717    5414  0 17:25 ?        00:00:00 /usr/bin/containerd-shim-runc-v2 -namespace moby -address /run/containerd/containerd.sock -publish-binary /usr/bin/containerd -id e8e6c64a4a060ce3667f2b2252c18040c179dc1fc778a62bb3dfbb733d837e25 start
root       16984   16969  0 17:22 ?        00:00:00 runc --root /var/run/docker/runtime-runc/moby --log /run/containerd/io.containerd.runtime.v2.task/moby/748309e2c3d7f394946014ea860e1d251596acca54fedb35e05328f0bf7ed8ad/log.json --log-format json create --bundle /run/containerd/io.containerd.runtime.v2.task/moby/748309e2c3d7f394946014ea860e1d251596acca54fedb35e05328f0bf7ed8ad --pid-file /run/containerd/io.containerd.runtime.v2.task/moby/748309e2c3d7f394946014ea860e1d251596acca54fedb35e05328f0bf7ed8ad/init.pid 748309e2c3d7f394946014ea860e1d251596acca54fedb35e05328f0bf7ed8ad
root       16994   16984  0 17:22 ?        00:00:00 runc init
root       17001   16984  0 17:22 ?        00:00:00 libnetwork-setkey -exec-root=/var/run/docker 748309e2c3d7f394946014ea860e1d251596acca54fedb35e05328f0bf7ed8ad e520fd115035
```

- on voit la création du `containerd-shim`
  - son rôle est de maintenir la communication entre le démon docker et le conteneur
- on voit aussi le lancement du *runtime* `runc` qui prépare le terrain pour l'exécution du conteneur

## 3. Isolation you said ?

**A ce stade, nous avons vu qu'un conteneur n'est qu'un simple processus** : une fois un conteneur lancé, le processus qu'il exécute est visible dans l'arborescence de processus de l'hôte avec un classique `ps -ef`.

> Accessoirement, vous avez sûrement remarqué que par défaut, TOUT tourne avec l'identité de l'utilisateur `root`.

Le démon Docker a pour but de préparer le terrain afin de créer un environnement dans lequel va s'exécuter le conteneur. Il utilise le mécanisme de *namespaces* pour isoler les processus qu'il lance et en faire des "conteneurs".

Dans cette partie, on va se concentrer à explorer l'isolation mise en place par les *namespaces*.

➜ **Visualiser l'isolation**

```bash
# assurez-vous qu'aucun conteneur ne tourne
$ docker ps
$ docker rm -f $(docker ps -aq)

# lancez un conteneur de test, qui sera facilement identifiable
$ docker run -d fedora sleep 999999

# visualiser l'isolation : on récupère un shell dans le conteneur
$ docker exec -it $(docker ps -lq) bash

# on a un shell dans le conteneur, explorons quelques aspects :
# réseau
$ ip a
# user
$ cat /etc/passwd
# process
$ ps -ef
# hostname
$ hostname
# d'autres aspects sont isolés, mais moins facilement visibles : la gestion de l'heure, ou les IPC
```

➜ **Les namespaces**

Les ***namespaces*** du noyau Linux sont les responsables de cette isolation. Quand un namespace est créé, un identifiant unique lui est attribué.

Il est possible à tout moment de consulter dans quels *namespaces* s'exécute un processus donné, en utilisant l'API `/proc/`. Let's go.

```bash
# consultons les identifiants des namespaces dans lesquels on évolue actuellement, avec le shell qu'on utilise actuellement
$ echo $$ # petit trick qui affiche le PID du shell actuel

# il existe dans /proc/ un dossier par processus, consultons le dossier concernant le shell actuel
$ ls /proc/$$/

# il y a ici un dossier ns, qui contient une référence vers les namespaces utilisés par notre shell actuel
$ ls -al /proc/$$/ns/

# la commande lsns permet d'afficher les namespaces actuels de la machine
$ lsns
```

**Grâce à cette manip, on peut identifier les IDs uniques des *namespaces* dans lesquels évolue un processus donné.**

Si deux processus évoluent dans un *namespace* `network` différent, par exemple, alors ils auront chacun une stack réseau différente (= ils n'auront pas les mêmes cartes réseau visibles avec un `ip a`).

Il en va de même avec le *namespace* `pid` par exemple, qui donnera à un processus donné une visibilité différente de l'arbre des processus (que l'on consulte usuellement avec la commande `ps -ef`).

> Si on utilise pas d'outils comme de la conteneurisation, tous les processus d'un système Linux évoluent par défaut dans les même namespaces.

➜ **Comprendre l'isolation**

```bash
# assurez-vous qu'aucun conteneur ne tourne
$ docker ps
$ docker rm -f $(docker ps -aq)

# lancez un conteneur de test, qui sera facilement identifiable
$ docker run -d fedora sleep 999999

# repérez les namespaces de votre shell actuel
$ ls -al /proc/$$/ns

# repérez le PID du processus sleep 999999 exécuté par le conteneur
$ ps -ef | grep 99999
# repérez les namespaces dans lesquels il évolue
$ ls -al /proc/<PID>/ns
```

**Vous devriez constater que vos namespaces et ceux du conteneurs sont différents.** Enfin, presque tous. hihi.

## 4. Capabilities

**Les *capabilities* déterminent des droits que peut avoir un processus pendant son exécution.**  
Des sortes de superpouvoirs qu'on peut lui ajouter. Ce sont habituellement des droits qui sont réservés à l'utilisateur `root` mais certains processus ont légitimement besoin de droits plus élevés pour s'exécuter.

> Comme la commande `ping` qui a besoin d'une capabilities spécifique afin d'envoyer des paquets ICMP.

➜ **Visualiser les capabilities**

```bash
# la commande getpcaps est là pour ça
# on repère le PID d'un processus
$ ps -ef

# visualisation des capabilities
$ getpcaps <PID>

# vous pouvez consulter une liste des capabilities et leur fonction à tout moment
$ man capabilities
```

➜ **Docker & capabilities**

Puisque donner toutes les *capabilities* à un processus équivaut à l'exécuter en tant que `root`, il est nécessaire de restreindre les *capabilities* des processus exécutés par les conteneurs.

```bash
# on lance deux conteneurs, avec un jeu de capa différente
$ docker run -d fedora sleep 99999
$ docker run -d --cap-drop cap_net_bind_service fedora sleep 99999

# on repère les PIDs des deux processus
$ ps -ef | grep 99999

# on repère les capabilities à disposition des processus
$ getpcaps <PID>
$ getpcaps <PID>

# la capa cap_net_bind_service devrait être absente du deuxième
```

> Bon, de toute façon, tout tourne avec `root` par défaut, alors comme qui dirait : xD saserarien

![Docker Security](../../../images/docker-security-is.jpg)

# II. Podman

> Va falloir enlever Docker de la machine pour que Podman puisse s'installer correctement.

➜ **Installer Podman sur la machine**

- pour l'install, référez-vous à la [doc officielle](https://podman.io/getting-started/installation)

➜ **Le CLI `podman`**

C'est le même que `docker`. Avec des trucs en plus.

➜ **Le démon `podman`**

Y'en a pas. Hihi.

➜ **Explorer `podman`**

Ré-utilisez tout ce qu'on on a vu Dans la partie Docker pour explorer Podman :

- regardez les process lancés par Podman, en particulier les utilisateurs qui lancent ces process
- regardez les namespaces utilisés par Podman

Vous devriez remarquer que l'empreinte de Podman est beaucoup plus légère sur le système, et qu'il n'a jamais besoin de `root` pour fonctionner.

![So hot](../../../images/container-security-so-hot-right-now.jpg)

➜ **La notion de *pods***

Podman tire son nom du fait que les *pods* sont natifs. Ceux qui sont familiers avec Kubernetes connaissent déjà cette notion, elle est similaire dans Podman.

Un Pod est ensemble de conteneurs, qui fonctionnent ensemble afin de fournir un service.  
L'exemple classique : un serveur web et une base de données, chacun dans un conteneur distinct. On pourra alors les placer dans le même *pod*.

Cela permet ensuite de gérer cet ensemble de conteneurs comme une seule entité.

```bash
# création d'un pod
$ podman pod create --name meo

# ajout de conteneurs dans le pod
$ podman run -d --pod meo nginx
$ podman run -d --pod meo nginx

# interactions avec le pod
$ podman pod ls
$ podman pod logs meo
$ podman pod stop meo
$ podman pod start meo

# on peut générer une unité systemd ou un manifest kubernetes à partir d'un pod existant
$ podman generate systemd meo
$ podman generate kube meo
```
