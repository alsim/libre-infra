# Containers tooling : Skopeo, Buildah, Falco, Trivy

Au menu, 4 petits tools pour s'armer dans le monde de la conteneurisation :

- **Skopeo** : inspection d'images
- **Buildah** : construction d'images
- **Trivy** : analyse de vulnérabilités dans les images
- **Falco** : détection d'anomalies dans le comportements des conteneurs

> Tous ces outils sont utilisés aussi bien manuellement que dans des processus automatisés comme du déploiement, de la CI/CD ou encore un registre d'images qui effectuent des actions automatiques.

- [Containers tooling : Skopeo, Buildah, Falco, Trivy](#containers-tooling--skopeo-buildah-falco-trivy)
- [I. Skopeo](#i-skopeo)
- [II. Buildah](#ii-buildah)
- [III. Trivy](#iii-trivy)
- [IV. Falco](#iv-falco)

# I. Skopeo

➜ **Installer Skopeo**

- suivez la [doc officielle](https://github.com/containers/skopeo/blob/main/install.md)

➜ **Utiliser Skopeo**

```bash
# la commande principale est skopeo inspect

# infos sur une images et les tags disponibles
$ skopeo inspect docker://docker.io/alpine
$ skopeo inspect docker://docker.io/nginx

# infos détaillées sur une image donnée
$ skopeo inspect --config docker://docker.io/alpine:latest
$ skopeo inspect --config docker://docker.io/nginx:latest
```

Tout est fait à distance, sans télécharger l'image localement.

# II. Buildah

Buildah est un outil utilisé pour créer des images compatibles avec le standard OCI. Il n'a pas besoin de l'utilisateur `root` pour fonctionner, ou d'un démon : c'est un simple outil en ligne de commande.

Il peut être utilisé de deux façons :

- en construisant un `Dockerfile` et en demandant à Buildah de construire l'image
- directement depuis la ligne de commande, c'est ce que nous allons voir ici

On va construire une p'tite image ensemble avec Buildah :)

➜ **Installer Buildah**

- en suivant la [doc officielle](https://github.com/containers/buildah/blob/main/install.md)

➜ **Créer une image avec le CLI Buildah**

```bash
# on pourrait directement lancer la commande 'buildah from alpine' mais cette syntaxe facilite les commandes qui vont suivre
$ ct=$(buildah from alpine)

# on install nginx dans l'image
$ buildah run $ct apk update
$ buildah run $ct apk install nginx

# on définit une commande qui permettra d'exécuter nginx quand on lancera le conteneur
$ buildah config --entrypoint '["nginx", "-g", "daemon off;"]' $ct

# on crée l'image
$ buildah commit $ct nginx:test

# on peut visualiser l'image créée
$ buildah images
$ podman images

# et lancer le conteneur
$ podman run -d -p 8888:80 nginx;test
$ curl localhost:8888
[...]
```

# III. Trivy

➜ **Installer Trivy**

- en suivant [la doc officielle](https://aquasecurity.github.io/trivy/v0.18.3/installation/)

On va survoler Trivy ici, dans le cadre de scan de vuln sur des images. Sachez qu'il est capable aussi d'analyser des Dockefiles, des fichiers liés à Kubernetes, ou des arborescences de projets entiers à la recherche des fichiers qu'il traite.

➜ **Scan de vuln avec Trivy**

```bash
$ trivy image alpine:latest

# y'a des images qu'il ne faut plus utiliser :^)
$ trivy image python:2.7 | head -n 8
2022-03-09T19:40:21.737+0100    INFO    Detected OS: debian
2022-03-09T19:40:21.737+0100    INFO    Detecting Debian vulnerabilities...
2022-03-09T19:40:21.888+0100    INFO    Number of language-specific files: 1
2022-03-09T19:40:21.888+0100    INFO    Detecting python-pkg vulnerabilities...

python:2.7 (debian 10.3)
========================
Total: 3256 (UNKNOWN: 6, LOW: 1224, MEDIUM: 1048, HIGH: 845, CRITICAL: 133)

# et c'est pas que les vieux trucs
$ trivy image python:latest | head -n 8
2022-03-09T19:44:56.365+0100    INFO    Detected OS: debian
2022-03-09T19:44:56.365+0100    INFO    Detecting Debian vulnerabilities...
2022-03-09T19:44:56.515+0100    INFO    Number of language-specific files: 1
2022-03-09T19:44:56.515+0100    INFO    Detecting python-pkg vulnerabilities...

python:latest (debian 11.2)
===========================
Total: 908 (UNKNOWN: 2, LOW: 534, MEDIUM: 225, HIGH: 122, CRITICAL: 25)
```

Tout est fait à distance, sans télécharger l'image localement.

![Vulns everywhere](../../../images/vulnerabilities-vulnerabilities-everywhere.jpg)

# IV. Falco

Falco permet de surveiller le comportement des conteneurs et d'effectuer des alertes en cas de comportement anormal.

Il effectue cette surveillance en regardant en temps réel les appels système qu'effectuent les conteneurs.

Là encore, nous allons survoler l'outil. Falco s'intègre aussi bien dans le cadre d'une surveillance de quelques conteneurs en production, qu'à une infrastructure Kube redondée.

➜ **Installer Falco**

- en suivant [la doc officielle](https://falco.org/docs/getting-started/installation/)

➜ **Première utilisation de Falco**

Par défaut, Falco est déjà configuré avec un certain nombre de règles qui détectent des comportement inhabituels pour des conteneurs de production. On peut mettre ça à l'épreuve assez facilement.

```bash
# démarrez le démon Falco
$ sudo systemctl start falco

# par défaut, Falco écrit en sortie d'erreur/standard
# on va donc visualiser les alertes la commande journalctl
$ sudo journalctl -xe -u falco -f

# dans un autre terminal, on va générer une alerte
$ podman run -it alpine sh
# vous devriez avoir une alerte générée par Falco
```

> En effet, il est rare d'avoir un conteneur qui ouvre un terminal avec un TTY attaché en production c:

![Much Slow c:](../../../images/doge-strace.jpg)
