# Hardening

Commencez par parcourir le guide CIS lié à votre distrib préférée, pour des bonnes pratiques générales.

> [Vous pourrez les trouver ici.](https://www.cisecurity.org/cis-benchmarks)

Les guides pour les bases RedHat (Rocky Linux, CentOS, RHEL, etc) sont réputés depuis longtemps.

- [syscalls](./syscalls.md)
- [Contrôle de conformité](./conformite.md)
- [SELinux](./selinux.md)
