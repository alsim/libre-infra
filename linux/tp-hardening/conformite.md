# Contrôle de Conformité

## 1. First steps

➜ **Lancer un machine virtuelle Rocky 9**

- ou autre chose, mais il faudra être un peu + autonome sur l'install et la suite des opérations
- avec Vagrant : `vagrant init generic/rocky9` -> `vagrant up`

➜ **Installez les paquets suivants** :

- `openscap`
- `openscap-scanner`
- `openscap-utils`
- `scap-security-guide`

➜ Il existe des **règles de conformité** pré-écrites et à notre disposition librement

- choisissez un profil (un jeu de règles) que vous voulez tester
- j'ai testé pour ce cours le profil de l'ANSSI niveau *intermediate*
- pour avoir la liste des profils disponibles, exécutez la commande suivante

```bash
# rl9 pour Rocky Linux 9, les règles sont adaptées à notre OS
oscap info /usr/share/xml/scap/ssg/content/ssg-rl9-ds.xml
```

> Vous pouvez trouver un exemplaire [d'un guide officiel de ce genre de l'ANSSI ici](https://www.ssi.gouv.fr/uploads/2019/02/fr_np_linux_configuration-v2.0.pdf).

➜ **Effectuez un contrôle de conformité** sur la machine

- on va utiliser une commande qui produit un rapport au format HTML
- utilisez la commande suivante

```bash
# la commande produira un fichier report.html dans le dossier courant
sudo oscap xccdf eval --report report.html --profile <PROFIL_CHOISI> /usr/share/xml/scap/ssg/content/ssg-rl9-ds.xml
```

- vous pouvez récupérez le rapport `report.html` sur votre machine et le visualiser dans votre navigateur web
- baladez-vous un peu, observez ce qui a été détecté
- depuis cette interface on peut extraire des playbooks Ansible de remédiation déjà

## 2. Remédiation avec Ansible

Pour la remédiation, on va pas non plus à chaque fois passer par un rapport HTML.

Il est possible de générer un *playbook* Ansible complet de remédiation.

➜ **Pour ce faire :**

```bash
# production d'un rapport de contrôle de conformité
oscap xccdf eval --profile <PROFIL_CHOISI> --results report.xml /usr/share/xml/scap/ssg/content/ssg-rl9-ds.xml

# production du playbook Ansible de remédiation
# cette commande produira un playbook Ansible remediation.yml dans le dossier courant
oscap xccdf generate fix --fix-type ansible --profile <PROFIL_CHOISI> --output remediation.yml report.xml
```

➜ **Intégrer le playbook de remédiation à votre dépôt Ansible**

- récupérer le fichier `remediation.yml`
- utiliser une commande `ansible-playbook` pour déployer la conf sur la machine
- il sera peut-être nécessaire d'adapter deux-trois ptits trucs pour que ça déroule mais ça se fait bien :)

> On peut facilement intégrer ce playbook de remédiation dans une infrastructure déjà gérée avec des outils de déploiement comme Ansible.