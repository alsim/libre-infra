
# SELinux

## 1. Exploration

### A. Config

Commandes SELinux :

```bash
# Etat de SELinux
$ sestatus
$ getenforce
```

Le fichier de conf principal de SELinux se trouve dans `/etc/selinux/config`.

### B. The Z option

La plupart des commandes possèdent, dans leur version moderne, une option `-Z` qui va nous permettre de lister les attributs SELinux de différents objets du système.

```bash
$ ls -al -Z
$ ps -ef -Z
$ id -Z
...
```

On utilise ces options en permanence pour vérifier l'état du système.

## 2. Simple example

➜ **Assurez-vous que SELinux est activé en mode `enforcing`**

➜ **Installez Apache (paquet `httpd`)**

➜ **Conf Apache, vitefé**

- supprimez le fichier `/etc/httpd/conf.d/welcome.conf`
- créez un fichier `/var/www/html/index.html`
- démarrez Apache `sudo systemctl start httpd`
- vérifiez que vous pouvez requêter votre fichier `curl localhost`

➜ **SELinux ?**

- vérifier le label des processus Apache
- vérifier le label du fichier dans `/var/www/html`

➜ **Getting denied**

- créer un fichier `/srv/html/index.html` avec le contenu de votre choix
- vérifiez son label : il est différent
- modifier la conf Apache `/etc/httpd/conf/httpd.conf` pour qu'il utilise le dossier `/srv/html` au lieu de `/var/www/html`
- restart : `sudo systemctl restart httpd`
- `curl localhost` : *403 Forbidden*

Lorsqu'une règle SELinux est enfreinte, on appelle cet évènement un *avc* et il est loggé :

```bash
$ sudo cat /var/log/audit/audit.log | grep avc
$ sudo sealert -a /var/log/audit/audit.log
...
```

Il est possible de changer le label de `/srv/html/index.html` à la main avec la commande `chcon`.  
Mais étant donné qu'on sait que le répertoire `/var/www/html/` avait le bon label, on peut se contenter de demander à `chcon` de copier le label de `/var/www/html` vers `/srv/html` :

```bash
# vérification des labels avant opération
$ ls -alZ /srv/html
$ ls -alZ /var/www/html
# on copie les labels d'un dossier, vers un autre
$ sudo chcon --reference /var/www/html /srv/html
# vérification des labels après opération
$ ls -alZ /srv/html
```

> Il aurait été aussi possible de générer automatiquement une règle pour whitelister cette interaction, à partir des logs, avec `cat /var/log/audit/audit.log | audit2allow`. On préfère pour cette technique passer SELinux en mode `permissive` afin de générer tous les logs pendant le fonctionnement de l'application. Ceci permet de générer une politique exhaustive pour le fonctionnement normal de l'application.

**Pour aller plus loin**, je vous recommande de suivre [le très bon et récent guide gentoo](https://wiki.gentoo.org/wiki/SELinux/Tutorials).
