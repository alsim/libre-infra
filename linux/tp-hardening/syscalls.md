# I. Syscalls

## 1. Exploration

La commande `strace` permet de suivre en temps réel les *syscalls* que passe un programme pendant son exécution.

Son utilisation est très simple :

```bash
# Afficher les syscalls effectués par un programme
$ strace <COMMAND>
$ strace -c <COMMAND>

# strace un processus existant, il faut connaître son PID
$ strace -p <PID>

# on peut enregistrer la sortie de strace plutôt que l'afficher en sortie standard
$ strace -o /path/to/file <COMMAND>
```

## 2. Logs

### A. Intro auditd

Pour obtenir des logs sur les *syscall* passés au sein de l'OS, de façon passive, on va utiliser le démon `auditd` présent par défaut sur les systèmes RedHat.

Vérifier qu'il est activé :

```bash
$ systemctl status auditd
$ sudo audictl -s
$ sudo audictl -l
$ auditctl --help
...
```

Les fichiers liés à `auditd` :

- la conf : `/etc/audit/`
- les logs générés :
  - consultables avec `ausearch`
  - stockés dans `/var/log/audit/audit.log` par défaut

### B. Conf auditd

On peut tester/ajouter de la conf depuis la ligne de commande avec `auditctl` :

```bash
# Surveille les appels au syscall listen (appelé quand un programme demande à écouter sur un port))
$ sudo auditctl -a always,exit -S listen -k key=suspect_command -F arch=b64

$ sudo auditctl -l
```

Dans cette commande :

- `-a` indique d'ajouter la commande
  - on précise en argument à quelle chaîne l'ajouter, ici `always` et `exit` indiquent qu'on va logger dès qu'un appel système commence son exécution, dès qu'il est appelé
- `-S` indique l'appel système que l'on souhaite surveiller
- `-k` permet de tagger l'alerte
  - `key=suspect_command` est un tag arbitraire, nous permettant de gérer les logs plus facilement
  - `arch=b64` indique l'architecture ciblée

Pour gérer les règles *via* un fichier de conf, il est possible d'écrire les règles directement dans un fichier du dossier `/etc/audit/rules.d/`, avec le contenu suivant :

```bash
-a always,exit -S execve -k key=suspect_command -F arch=b64
```

> Ce sont simplement les commandes, sans préciser `auditctl` devant.

## 3. Intégration

### A. systemd

Dans une unité *systemd* il est possible d'utiliser la clause `SystemCallFilter=` afin de préciser une liste de *syscalls* qui sont autorisés.

Si un *syscall* non-autorisé est utilisé par le processus, il sera tué.

> Voir [la page de doc dédiée](https://www.freedesktop.org/software/systemd/man/systemd.exec.html#SystemCallFilter=).

La commande `systemd-analyze security` déjà abordée demande la restriction de l'utilisation des syscalls.

### B. Docker

Il est possible d'associer une politique *seccomp* à chaque conteneur à son lancement, ou bien définir une politique *seccomp* globale utilisée par défaut à chaque lancement de conteneurs.

Sur des systèmes modernes, Docker fournit un profil *seccomp* par défaut, qui bloque un certains nombres d'appels système, mais reste surtout compatible avec beaucoup d'applications. Il est nécessaire de compléter ce profil *seccomp* avec des règles maison pour renforcer la sécurité des conteneurs Docker.

> Voir [la page de doc dédiée](https://docs.docker.com/engine/security/seccomp/)
