# Advanced Features

Partie + modulaire, chaque partie est indépendante et vise à vous faire découvrir plusieurs fonctionnalités annexes, +

# Sommaire

- [Advanced Features](#advanced-features)
- [Somamire](#somamire)
- [I. OS boot](#i-os-boot)
- [II. Service hardening](#ii-service-hardening)
- [III. D-bus](#iii-d-bus)
  - [1. D-Bus](#1-d-bus)
- [IV. CGroups](#iv-cgroups)
- [V. Quelques dernières commandes](#v-quelques-dernières-commandes)

# I. OS boot

➜ **Plusieurs outils en ligne de commande permettent d'avoir des informations sur le dernier boot :**

- `systemd-analyze time` : affiche le temps de boot de l'OS
- `systemd-analyze blame` : affiche les *unités* en cours de fonctionnement, triés par ordre de vitesse de démarrage
- `systemd-analyze plot > file.svg` : génère un diagramme au format SVG qui représente le boot de l'OS. Très pratique.

# II. Service hardening

Bon c'est cool pour cette partie, j'vais pas avoir besoin de m'étendre en palabres et donner des exemples étou, *systemd* va le faire à ma place.

Il existe plusieurs clauses que l'on peut ajouter dans un fichier qui définit une *unité service* pour améliorer sa sécurité. Il existe une commande *systemd* pour analyser un fichier d'*unité service* et déterminer ce qu'il manque pour améliorer sa sécurité.

Cette commande est `systemd-analyze security <UNIT>`. Sont alors listés un certain nombre de clauses qu'il est possible d'ajouter au fichier.  
Un score est aussi attribué à l'*unité* qui permet de cerner son niveau de sécurité.

➜ **Obtenez le score le plus haut possible en partant de l'unité suivante :**

```systemd
[Unit]
Description=Python simple webserver

[Service]
Type=simple
WorkingDirectory=/var/www/html
ExecStart=/usr/bin/python -m http.server 8080

[Install]
WantedBy=multi-user.target
```

# III. D-bus

## 1. D-Bus

D-Bus est un outil de communication inter-processus. Il est très utilisé, et de plus en plus, au sein des systèmes GNU/Linux. Il fonctionne sur un système d'évènements et d'abonnements.

Pour faire simple : un programme peut créer un *bus de communication*, qui permet à d'autres programmes de lui poser des questions. On peut appeler des méthodes, ou récupérer la valeur de certaines propriétés.

Pour les hipsters : c'est comme une API, mais interne au système, et avec un système d'évènements auxquels on peut s'abonner.

➜ **Exploration de D-Bus sur la ligne de commande avec `busctl` :**

```bash
# on liste les bus actifs sur la machine
$ busctl list # on repère le nom d'un bus, colonne "NAME"

# on peut afficher les infos au sujet d'un bus spécifique
$ busctl tree <BUS>
# par exemple
$ busctl tree org.freedesktop.NetworkManager
# on voit alors toutes l'arbre correspondant à un service donné

# chacun de ces objets nous fournit des méthodes et des propriétés
# pour voir ce qui est disponible
$ busctl introspect <BUS> <OBJECT>
$ busctl introspect org.freedesktop.NetworkManager /org/freedesktop/NetworkManager/IP4Config/1

# on peut alors, par exemple, récupérer une propriété donnée
$ busctl get-property <BUS> <OBJECT> <INTERFACE> <PROPERTY
# par exemple
$ busctl get-property org.freedesktop.NetworkManager /org/freedesktop/NetworkManager/IP4Config/4 org.freedesktop.NetworkManager.IP4Config Routes
>
```

Essayez d'explorer un peu par vous-mêmes, on peut récupérer plein d'infos intéressantes.

> Pour les p'tits hackers, y'a eu quelques vulns qui ont tourné autour de D-Bus. C'est un point critique du système, qui entretient la communication, parfois d'informations sensibles, entre les processus.

# IV. CGroups

Il est possible d'apposer des restrictions d'accès à un groupe de processus en créant un *slice* systemd.

➜ **Créer un nouveau *slice**

- il doit contenir 3 services (peu importe, pour le test)
- vous pouvez monitorer le *slice* avec `systemd-cgls` et `systemd-cgtop`
- créer le fichier `/etc/systemd/system/test.slice` :

```systemd
[Unit]
Description=test slice
Before=slices.target

[Slice]
MemoryAccounting=true
IOAccounting=true
CPUAccounting=true

MemoryHigh=4%
MemoryLimit=5%
CPUShares=10
```

- créer un fichier de service qui utilise le *slice* :

```systemd
[Service]
...
Slice=test.slice
```

- démarrer le service, et vérifier le comportement avec `systemd-cgtop`

> Il existe beaucoup d'autres options de restrictions, **voir [la doc officielle](https://www.freedesktop.org/software/systemd/man/systemd.resource-control.html#) pour + de détails.**

# V. Quelques dernières commandes

La commande `systemd-nspawn` permet d'exécuter des processus sandboxés, à la façon des conteneurs.

L'outil est généralement utilisé comme un `chroot` mais plus puissant, car mettant en place de l'isolation avec les namespaces du noyau.

> Je vous laisse vous amuser un peu avec la commande et son `man`.

---

On peut créer des fichier et dossier temporaires avec la commande `systemd-tmpfiles`. Très utile pour du scripting.

---

Une dernière : `systemd-analyze --user dump` qui affiche un rapport détaillé de l'état du système. La commande `systemd-analyze` fait toute sorte de trucs marrants.
