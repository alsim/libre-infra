# Systemd Management

## Sommaire

- [Systemd Management](#systemd-management)
  - [Sommaire](#sommaire)
  - [0. Intro](#0-intro)
  - [1. Services](#1-services)
  - [2. Targets](#2-targets)
  - [II. System Management](#ii-system-management)
    - [1. Name Resolution](#1-name-resolution)
    - [2. Network](#2-network)
    - [3. Locale](#3-locale)
    - [4. Login](#4-login)
    - [5. Hostname](#5-hostname)
    - [6. Time](#6-time)

## 0. Intro

➜ **Quelques commandes liées aux unités**

```bash
# lister les unités
$ systemctl list-units # uniquement les unités actives
$ systemctl list-units --all
$ systemctl list-units --type=service
$ systemctl list-unit-files # liste les fichiers de définition d'unités

# afficher les dépendances d'une unité donnée
$ systemctl list-dependencies <UNIT>

# gérer l'état d'une unité
$ systemctl start <UNIT>
$ systemctl stop <UNIT>
$ systemctl restart <UNIT>

# gérer l'état d'une unité
$ systemctl status <UNIT>

# afficher le path et le contenu du fichier qui définit l'unité
$ systemctl cat <UNIT>

# éditer le fichier d'unité
$ systemctl edit --full <UNIT>

# commande à effectuer à chaque modif d'un fichier d'unité
$ systemctl daemon-reload
```

## 1. Services

> A chaque fois qu'il sera demandé de créer une *unité*, il faudra donc créer un fichier dans `/etc/systemd/system/`.

Afin d'aérer un peu l'espace, [un TP est dédié aux services](./service_management.md).

## 2. Targets

Un *target* est un regroupement d'autres unités. Chaque unité d'un *target* se voit munie d'une section `[Install]` en bas de fichier qui indique à quel *target* elle appartient.

➜ **Créer un nouveau *target***

- installez deux services : NGINX et MariaDB
- utilisez un *target* pour grouper ces deux unités
- vérifiez que vous pouvez démarrer tous ces *services* d'un coup, en démarrant le *target*

## II. System Management

### 1. Name Resolution

C'est avec la commande `resolvectl` qu'on peut utiliser la résolution de nom.

Plusieurs fonctionnalités avancées sont liées à l'utilisation du démon `systemd-networkd` afin de gérer les requêtes DNS effectuées par la machine.

➜ **Mettez en place une résolution de nom DNS over TLS**

- afin d'améliorer la confidentialité des données émises par la machine
- il vous faudra utiliser le service `systemd-resolved` (pas démarré par défaut sur certaines distrib)
  - la conf est dans `/etc/systemd/resolved.conf`
  - utilisez un DNS compatible, comme celui de CloudFlare : `1.1.1.1`

### 2. Network

Il faut que la machine utilise le service `systemd-networkd` pour autoriser la création d'unité de type *network*.

➜ **Assurez-vous que `systemd-networkd` est installé et actif sur la machine**

- avec une commande `systemctl status systemd-networkd`

➜ **Configurer une interface avec une IP statique à l'aide d'une unité *network***

- enfin une syntaxe claire et standard pour faire de la conf réseau
- standard à travers toutes les distribs :)
- exemple de syntaxe d'un fichier `/etc/systemd/network/enp0s8.network` :

```systemd
[Match]
Name=enp0s8

[Network]
Address=192.168.1.110/24
DNS=1.1.1.1
```

> La configuration possible est très fine et permet des choses vraiment avancées. **Référez-vous à [la doc officielle](https://www.freedesktop.org/software/systemd/man/systemd.network.html) pour + d'infos**.

### 3. Locale

On utilise le binaire `localectl` pour la gestion de locale.

### 4. Login

Le binaire `loginctl` est utilisé pour interroger les sessions utilisateurs.

### 5. Hostname

La commande `hostnamectl` est utilisée pour manipuler le nom de domaine de la machine.

### 6. Time

Un binaire `timedatectl` pour la gestion du temps.

- utilisez `timedatectl` pour :
  - vous assurer qu'un service NTP est démarré
  - changer de fuseau horaire