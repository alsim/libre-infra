

TP modeste pour appréhender quelques outils autour du tracing.

Aussi bien le tracing d'utilisation ressources système, ou le tracing de l'exécution d'une fonction spécifique dans une application donnée, les systèmes Linux peuvent être équipés d'outils modernes pour profiter de ces fonctionnalités.

Dans ce TP, on va voir deux choses : quelques outils en ligne de commandes, et un lab simple autour de Pyroscope, ainsi qu'un petit lancement de Skydive.

- [Tracing](#tracing)
- [I. CLI tools](#i-cli-tools)
  - [1. Classics](#1-classics)
  - [2. eBPF](#2-ebpf)
    - [execsnoop](#execsnoop)
    - [opensnoop](#opensnoop)
    - [ext4slower](#ext4slower)
    - [tcpconnect](#tcpconnect)
    - [Autres](#autres)
- [II. Pyroscope](#ii-pyroscope)
- [III. Skydive](#iii-skydive)

# I. CLI tools

## 1. Classics

Je vais vous renvoyer vers une excellente ressource qui est le blog  de NetFlix (ce blog est un excellent livre de chevet pour tout technicien système) : https://netflixtechblog.com/linux-performance-analysis-in-60-000-milliseconds-accc10403c55

On pensera à rajouter le très utile `journalctl` (qui permet de voir les logs noyaux de `dmesg` avec un `journalctl -k` mais en profitant des features de `journalctl`).

Aussi, exécuter `ss` pour observer les connexions en cours :

- `sudo ss -tupnl` pour les process en écoute TCP et UDP
- `sudo ss -tunp` pour les proces client TCP et UDP

## 2. eBPF

**Prérequis** : installer BCC sur l'OS Linux que vous utilisez pour les tests.

Les outils montrés dans cette partie sont disponibles après installation de BCC. Soit dans votre PATH directement, soit dans `/usr/share/bcc/tools`.

### execsnoop

```
# cd /usr/share/bcc/tools
# ./execsnoop
```

`execsnoop` permet de tracer l'exécution de nouveaux processus. Il permet notamment de repérer l'exécution de processus qui ont une durée de vie très courte. Il trace l'appel système `exec()`.

### opensnoop

```
# ./opensnoop
PID    COMM               FD ERR PATH
1565   redis-server        5   0 /proc/1565/stat
1565   redis-server        5   0 /proc/1565/stat
1565   redis-server        5   0 /proc/1565/stat
1603   snmpd               9   0 /proc/net/dev
1603   snmpd              11   0 /proc/net/if_inet6
1603   snmpd              -1   2 /sys/class/net/eth0/device/vendor
1603   snmpd              11   0 /proc/sys/net/ipv4/neigh/eth0/retrans_time_ms
1603   snmpd              11   0 /proc/sys/net/ipv6/neigh/eth0/retrans_time_ms
1603   snmpd              11   0 /proc/sys/net/ipv6/conf/eth0/forwarding
[...]
```

`opensnoop` trace les appels au syscall `open` et va donc nous indiquer tous les nouveaux fichiers ou espace en RAM qui vont être accédé en lecture ou écriture à partir du lancement de la commande.

### ext4slower

```
# ./ext4slower
Tracing ext4 operations slower than 10 ms
TIME     COMM           PID    T BYTES   OFF_KB   LAT(ms) FILENAME
06:35:01 cron           16464  R 1249    0          16.05 common-auth
06:35:01 cron           16463  R 1249    0          16.04 common-auth
06:35:01 cron           16465  R 1249    0          16.03 common-auth
06:35:01 cron           16465  R 4096    0          10.62 login.defs
06:35:01 cron           16464  R 4096    0          10.61 login.defs
```

`ext4slower` permet de repérer les opérations sur le filesystem qui prennent plus d'un certain temps. Il est idéal pour repérer des processus bloquants ou suspects sur la machine.

### tcpconnect

```
# ./tcpconnect
PID    COMM         IP SADDR            DADDR            DPORT
1479   telnet       4  127.0.0.1        127.0.0.1        23
1469   curl         4  10.201.219.236   54.245.105.25    80
1469   curl         4  10.201.219.236   54.67.101.145    80
1991   telnet       6  ::1              ::1              23
2015   ssh          6  fe80::2000:bff:fe82:3ac fe80::2000:bff:fe82:3ac 22
[...]
```

`tcpconnect` permet d'afficher les connexions TCP actives en cours sur la machine. On obtient aussi quelques détails supplémentaires comme la commande qui a demandé l'accès réseau.s

### Autres

Il existe de nombreuses autres commandes disponibles pour obtenir des données supplémentaires sur le système à l'aide des tools BCC, amusez vous à en tester quelques uns :

![BCC tooling](./pics/bcc_tracing_tools.png)

# II. Pyroscope

Pyroscope est un outil en plein de développement et a été racheté par Grafana il y a 1 semaine au moment de l'écriture de ces lignes (le 15 Mars 2023).

Son but est de fournir une observabilité profondément intégrée avec les applications et ainsi pouvoir tirer des métriques précises et sur-mesure en fonction des langages.

Ainsi, Pyroscope va nous permettre d'observer notamment l'utilisation RAM et la demande de temps CPU de chaque fonction d'un bout de code donné. On trouve notamment Go et Python dans les langages supportés.

---

Voilà de quoi le tester rapidement avec un bout de code Python qui déploie une API avec FastAPI :

Créez un dossier avec les fichiers suivants :

- `requirements.txt`

```
uvicorn
fastapi
pyroscope-io
```

- `src/main.py`

```python
from fastapi import FastAPI
import pyroscope

pyroscope.configure(
  application_name = "opensource_app", 
  server_address   = "http://pyroscope-server:4040/", 
)

app = FastAPI()

@app.get("/")
async def root():
    with pyroscope.tag_wrapper({ "controller": "long_print" }):
        for i in range(0, 1000000):
            print(i)
    return {"message": f"Long print issued {i} times"}
```

- `Dockerfile`

```Dockerfile
FROM python

RUN mkdir /app

COPY requirements.txt /app

COPY ./src /app

WORKDIR /app

RUN pip install -r requirements.txt

CMD ["uvicorn", "main:app", "--reload", "--host", "0.0.0.0"]
```

- `docker-compose.yml`

```yml
version: "3"

services:
  pyroscope-server:
    image: pyroscope/pyroscope
    ports:
      - 4040:4040
    command: server
    networks:
      pyro:

  python-app:
    image: test-app
    ports:
      - 8000:8000
    networks:
      pyro:

networks:
  pyro:
```

Puis effectuez les commandes suivantes :

```bash
$ docker build . -t test-app
$ docker compose up
```

RDV sur `http://localhost:4040` pour l'interface de Pyroscope. Vous pouvez faire une requête sur l'app Python en tapant sur `http://localhost:8000` et obtenir quelques métriques.

> N'oubliez pas que Pyroscope est en développement actif !

# III. Skydive

Skydive est un outil de **métrologie et d'observabilité réseau**. Il repose beaucoup sur eBPF pour récolter des métriques sur le trafic entre les différentes interfaces réseau.

Il est adapté pour fonctionner dans des infrastructures distribuées, typiquement dans des cluster Kubernetes, mais peut très bien fonctionner sur un noeud simple.

Il est **dynamique** et permet d'avoir un oeil en temps réel, d'une façon très human-readable, sur le trafic réseau qui circule au sein d'un noeuds ou entre plusieurs machines.

On va ici se contenter du *getting started*, le but reste de vous donner un tour d'horizon des pouvoirs d'eBPF.

```
docker run -d --privileged --pid=host --net=host -p 8082:8082 -p 8081:8081 \
    -e SKYDIVE_ANALYZER_LISTEN=0.0.0.0:8082 \
    -v /var/run/docker.sock:/var/run/docker.sock -v /run/netns:/var/run/netns \
    skydive/skydive allinone
```

Et rdv sur `http://localhost` pour la balade. Il est préférabl d'avoir plusieurs conteneurs lancés sur votre machine pour avoir des trucs à regarder. Encore mieux s'il y a du trafic entre eux pour visualiser le trafic en temps réel.
