# Présentation de la majeure

[Descriptif](./descriptif.md) du cours

- [Open source vs logiciel libre](./logiciel-libre.md)
- [Open source en france](./open-source-en-france.md)
- [Présentation du projet](./projet.md)

## Contexte

- [rappels GNU/linux](../linux/rappels-linux.md)
- Principes de [gestion d'une infrastructure](./gestion-infra-run.md)
- La [maturité des systèmes d'informations](./maturite-SI.md)
- Description d'une platforme de service : [le DAT](./description-infra.md)
