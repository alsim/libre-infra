# Open Monitoring Distribution

## Présentation

OMD est une distribution logicielle dédié au monitoring. Au départ developpée en open source, cette solution est  maintenant aussi distribuée en version entreprise (fermé).

La version open sources offre déja un grand nombre de fonctionalité. La principale focntion est la découverte automatique de services.

Le point for de cet outil est, plutot que de proposer une interface de configuration des hosts et service, check_mk propose de définir des règles de découverte / configurations des service.

## Installation

Voir le vagrant file, et la doc : https://checkmk.com/cms_introduction_packages.html

Après le Vagrant up, la solution est en place sur l'url : http://192.168.33.5:8081/lab_local/check_mk/ 

login/pass **cmkadmin/cmkadmin** (ouais je sais)

Un site lab.local est créé et légèrement configuré, mais rien n'est monitoré pour le moment

## Configuration

Wato, l'outil de configuration offre plusieurs solutions pour organiser sa configuration très utile dans la cadre de la surveillance d'une infrastructure de taille importante et hétérogène :

- Une arborescence de dossier
- Des tags
- Des labels

> tite doc : https://checkmk.com/cms_wato.html

## Un petit TD

clonez le repo git : git@gitlab.com:alsim/tp-monitoring.git

### L'environement

Afin de tester une solution de monitoring il faut une infra, j'ai posé quelques hosts rapidement à partir des TP précédents :

![](../images/tp-check_mk.png)

Alors oui, une config avec ansible cela serais plus propre plus facilement maintenable, mais cela n'était pas l'objectif, il nous fallais quelques chose de trés simple à mettre en oeuvre car ce n'est pas l'objet du TD.

```bash
adminhost$ vagrant up
```

Le déploiement prends entre 15 et 20 minutes.

L'installation de l'outil est fait à partir d'une seul package rpm (ou deb) [dwl page](https://checkmk.com/download.php?edition=cre&version=stable&dist=redhat&os=el7) mais il est déja intégré dans le dépot du TD pour simplifier.

> [quick start doc](https://checkmk.com/cms_introduction_packages.html)

### Analyse de la configuration

Pour chaque site défini, créée un compte du nom du site, on l'administre avec ce compte et la commande omd : 

```bash
adminhost$ vagrant ssh mon
Last login: Wed Dec 11 23:14:21 2019 from 10.0.2.2
[vagrant@mon ~]$ sudo su - lab_local
Last login: Wed Dec 11 23:14:45 UTC 2019 on pts/0
OMD[lab_local]:~$ omd status
mkeventd:       running
rrdcached:      running
npcd:           running
nagios:         running
apache:         running
crontab:        running
-----------------------
Overall state:  running
```

sa config est rudimentaire :
```bash
OMD[lab_local]:~$ omd config show
ADMIN_MAIL: 
APACHE_MODE: own
APACHE_TCP_ADDR: 192.168.33.5
APACHE_TCP_PORT: 8081
AUTOSTART: on
CORE: nagios
LIVESTATUS_TCP: off
MKEVENTD: on
MKEVENTD_SNMPTRAP: off
MKEVENTD_SYSLOG: off
MKEVENTD_SYSLOG_TCP: off
MULTISITE_AUTHORISATION: on
MULTISITE_COOKIE_AUTH: on
NAGIOS_THEME: exfoliation
NSCA: off
PNP4NAGIOS: on
TMPFS: on
OMD[lab_local]:~$ 
```

L'outil s'auto check : [analyse config](http://192.168.33.5:8081/lab_local/check_mk/index.py?start_url=%2Flab_local%2Fcheck_mk%2Fwato.py%3Ffolder%3D%26mode%3Danalyze_config) 
utilisez le bouton ![](../images/cmk-analyse-config.png)

on retrouve les alertes suivantes :

- Apache number of processes : dans les détail (i pour information) on vois un défaut dans la config check_mk ne peu pas se connecter au module serveur-status d'apache, c'est un peu bof mais bon, corrigeons :
  ```bash
  OMD[lab_local]:~$ sed -i.orig '$aListen 127.0.0.1:8081' etc/apache/listen-port.conf
  OMD[lab_local]:~$ diff etc/apache/listen-port.conf*
  5d4
  < Listen 127.0.0.1:8081
  OMD[lab_local]:~$ omd restart
  Removing Crontab...OK
  Stopping apache...killing 7521.......OK
  Stopping nagios.....OK
  Stopping npcd...OK
  Stopping rrdcached...waiting for termination...OK
  Stopping mkeventd...killing 7422...OK
  Starting mkeventd...OK
  Starting rrdcached...OK
  Starting npcd...OK
  Starting nagios...OK
  Starting apache...OK
  Initializing Crontab...OK
  OMD[lab_local]:~$ 
  ```
  Cette fois dans les détail on peu utiliser le lien [number of apache processes](http://192.168.33.5:8081/lab_local/check_mk/index.py?start_url=%2Flab_local%2Fcheck_mk%2Fwato.py%3Fmode%3Dedit_configvar%26varname%3Dapache_process_tuning) et 12 processus suffiront pour notre usage courant.
  (le voyant orange change apparait, on en reparle plus loin)
- on retourne sur l'analyse de la configuration sur la partie backup :
  - on va sur le lien backup sur la gauche : ![](../images/cmk-backup.png)
  - on défini une cible (target) locale sur **/opt/backups**  ([lien](http://192.168.33.5:8081/lab_local/check_mk/wato.py?mode=backup_targets))
  - On défini une clef de chiffrement : ([lien](http://192.168.33.5:8081/lab_local/check_mk/wato.py?mode=backup_keys)) que l'on sauvegardera en lieu sur
  - enfin on créé un backup job chiffré, tout les jour à 17h par exemple : ([lien](http://192.168.33.5:8081/lab_local/check_mk/wato.py?mode=backup))
- retournons sur lanalyse de la configuration et tout en bas, bon, on a pas mis en place le https mais bon pour notre lab et vu que l'interface n'est disponible que sur notre réseaux interne, on acknoledge (le bouton tout à droite) cette anomalie car on l'accepte.

Nous avons toujours le voyant orange : "1 change" allons y; en fait les modification ne sont pas prise en compte immédiatement, il faut appliquer les changement pour qu'ils soient pris en compte bouton vert activate affected. le changement (paramétrage du nombre de processus accepter sur la config apache de check_mk = 12 )

### Créez vous un compte

Je vous propose de vous créer un compte, admin bien sur et de vous reconnecter avec (bouton user dans le menu wato).

Je vous recommande le thème modern dark qui est bien plus confortable pour les yeux.

> On en profite pour Cheker le compte "automation" et copier son token d'authentification.

### Deploiement du monitoring

Deployez l'agent check_mk sur les hosts de l'infra avec Vagrant :

```bash
adminhost$ vagrant provision --provision-with initmonit
==> mon: Running provisioner: initmonit (shell)...
    mon: Running: inline script
==> lb1: Running provisioner: initmonit (shell)...
    lb1: Running: inline script
==> lb2: Running provisioner: initmonit (shell)...
    lb2: Running: inline script
==> web1: Running provisioner: initmonit (shell)...
    web1: Running: inline script
==> web2: Running provisioner: initmonit (shell)...
    web2: Running: inline script
==> dns: Running provisioner: initmonit (shell)...
    dns: Running: inline script
```

Cela install l'agent check_mk à partir du package disponible sur le serveur de monitoring : 

```bash
yum -y -q install http://192.168.33.5:8081/lab_local//check_mk/agents/check-mk-agent-1.6.0p6-1.noarch.rpm 
```

Quelle est le socket ouvert par l'agent ?

Quel est le processus qui écoute sur ce socket ?

#### Deploiment manuel

- sur l'interface [wato hosts](http://192.168.33.5:8081/lab_local/check_mk/index.py)
- Bouton `new host`
- hostname : web1.lab.local
- bouton `Save & go to Service` (en bas)

Puis tout en bas, monitor, et enfin, on aplique les changements en haut.

#### Déploiement par l'API

utiliser le petit outil bash dans le dépot (il faudra disposer de curl et jq):

```bash
adminhost$ tools/monitor-host mon.lab.local
please set the wato api secret for user automation :
$ export watoapisecret="xxxxxxxxxxxxx"
adminhost$ export watoapisecret="f5685256-265f-4178-8d25-a67be88ad6a4"
adminhost$ tools/monitor-host mon.lab.local
add host api call result:
{
  "result": null,
  "result_code": 0
}

discover host api call result:
{
  "result": "Service discovery successful. Added 22, removed 0, kept 0, total 22 services and 0 new, 1 total host labels",
  "result_code": 0
}

apply config api call result:
{
  "result": {
    "sites": {
      "lab_local": {
        "_time_updated": 1576112820.753505,
        "_status_details": "Started at: 01:06:56. Finished at: 01:07:00.",
        "_phase": "done",
        "_status_text": "Success",
        "_pid": 10058,
        "_state": "success",
        "_time_ended": 1576112820.753505,
        "_expected_duration": 2.9505647102355965,
        "_time_started": 1576112816.217551,
        "_site_id": "lab_local",
        "_warnings": []
      }
    }
  },
  "result_code": 0
}
```

du coup et fait les 4 autres : 

```bash
adminhost$ tools/monitor-host lb1.lab.local > /dev/null
adminhost$ tools/monitor-host lb2.lab.local > /dev/null
adminhost$ tools/monitor-host ns1.lab.local > /dev/null
adminhost$ tools/monitor-host web2.lab.local > /dev/null
```

> L'api est plutôt simple à utiliser et pourra être utiliser donc pour ajouter des hosts à la volé avec l'outil de gestion de conf (delegate_to: 127.0.0.1). [api-references](https://checkmk.com/cms_web_api_references.html)

### Traitement des alarmes

Nous avons donc quelques alertes

web1 entre temps (dans mon cas) à découvert de nouvelles choses à monitorer, on edite alors ses services :

![](../images/cmk-edit-services.png)

Et on active simplement le service ntp qui à été découvert entre temps : 

![](../images/cmk-monitor.png)

On activera les changement dans la foulée (et on fera de même pour les suivants)

> cmk nous alerte dponc lorsque qu'il découvre de nouveaux services, et ça c'est plutot cool.

#### Acknowledgement

Vous les avez les alertes systemd ? enfin, moi je les ais.

Sur web1 et web2, pas les autres hosts nous avons le service systemd : `selinux-policy-migrate-local-changes@targeted` en statut failed.

**1 - Acknowledgement :**

J'active les check box et je selectione les deux alarme en même temps :

![](../images/cmk-ack-1.png)

Je choisi le marteau, puis je met en message sticky permanent sur l'alarme et y ajoutant le numéro de ticket (s'il y avais un ticket) et ce pour deux raisons.

![](../images/cmk-ack-2.png)

**2 - résolution :**

On a deux solutions:

- On corrige l'incident (c'est en faite un petit bug lié à l'environement) un upgrade de l'OS ou l'activation du port série sur la VM corrige le probleme mais dans les deus cas il faut reboot, on vas pas faire ça là maintenant.
- Ensuite cette Alarme n'est pas trés importante. Mais la laisser acknowledgé nous empèche de voir les potentiels autre problemes remonté par cette sonde. J'aimerais donc simplement **désactiver ce service pour ceux deux VM.**

Je vais dans Wato : hosts and service parameters / Parameters for discovered services

**C'est le point central de la config check_mk, la configuration du moteur de règle**

Je recherche systemd :

![](../images/cmk-rules-1.png)

Je choisi `systemd Services` puis `add a rule in folder`

je selection : `Exclude services matching provided regex patterns`

avec la paterne : "selinux-policy-migrate-local-changes@targeted"

avec la condition host explicite pour ces deux hosts

![](../images/cmk-rules-2.png)

le monitor passe alors au vert mais garde le message avec le numéro de ticket que j'ai bien sur agrémenté de commentaire expliquant la résolution.

![](../images/cmk-rules-3.png)

> L'intérêt de cette manip est de découvrir les Règles de paramétrage.

### ajout de plugins check_mk

#### Mysqld

[doc sur le plugin](https://checkmk.com/cms_legacy_mysql.html)

Sur le hosts qui porte la base de donnée :  (serveur dns) :
```sql
MariaDB [(none)]>  GRANT ALL PRIVILEGES ON *.* TO 'monitor'@'localhost' identified by 'MyPassWord';
```

Installation et configuration du plugins : 
```bash
[root@dns ~]## cat /etc/check_mk/mysql.cfg
[client]
user=monitor
password=MyPassWord
[root@dns ~]## curl -s -o /usr/lib/check_mk_agent/plugins/mk_mysql http://192.168.33.5:8081/lab_local/check_mk/agents/plugins/mk_mysql
[root@dns ~]## chmod +x /usr/lib/check_mk_agent/plugins/mk_mysql
```

#### Nginx

[doc sur le plugin](https://checkmk.com/cms_check_nginx_status.html)

Sur les hosts qui portent nginx : 

Ajoutez dans la conf nginx des serveur lb l'accès au module status d'nginx:

```bash
[root@lb1 ~]# grep -n /nginx_status /etc/nginx/nginx.conf
57:        location /nginx_status { stub_status on; access_log off; allow 127.0.0.1; allow ::1; deny all;}
[root@lb1 ~]# systemctl restart nginx.service 
```

Puis installer/configurez l'agent :

```bash
[root@lb1 ~]# cat /etc/check_mk/nginx_status.cfg 
# Example configuration for NGNIX plugin
servers = [
    {
       "protocol" : "http",
       "address"  : "localhost",
       "port"     : 80,
       "page"     : "nginx_status",
    },
]
[root@lb1 ~]# curl -s -o /usr/lib/check_mk_agent/plugins/nginx_status http://192.168.33.5:8081/lab_local/check_mk/agents/plugins/nginx_status
[root@lb1 ~]# curl -s -o /usr/lib/check_mk_agent/plugins/nginx_status http://192.168.33.5:8081/lab_local/check_mk/agents/plugins/nginx_status
[root@lb1 ~]# chmod +x /usr/lib/check_mk_agent/plugins/nginx_status
```

> Le monitor : `Check_MK Discovery` nous alertera lors de la découverte de ces nouveau service il nous suffira alors d'éditer les services du host pour les ajouter aux services surveillés \o/

> Il est possible de créer ses propre sondes en ajoutant des exécutable dans le dossier `/usr/lib/check_mk_agent/plugins/` ceux-ci doivent juste produire une ligne sur sa sortie standard sous la forme `RC Service - message` pour un message type de l'api nagios : RC le code retour 0 (ok), 1 (w), ou 2 (c); Service le nom du service, - pour pas de donnée de perf sinon 'perf=val1;val2;val3|count=val4' avec les valeurs de performances puis enfin un message qui lui accepte les espaces.

### Notifications

bah c'est un peu le but, là, on regarde l'interface mais l'objectif c'est de ne plus la regarder et d'être notifier lorsqu'on a besoin d'y revenir

Dans le menu wato : 
![](../images/cmk-notif-0.png)

On trouve un joli warning, je vous laisse lire et traiter le point.

Sinon une règle générique viens notifier tout les contact défini en tant que contact sur les services.

chaque service dispose des parmètres suivant : 

```
Service contact groups	check-mk-notify, all
Service contacts	check-mk-notify
```

Nous pouvons donc administrer les `contact group` via le menu wato associé : 
![](../images/cmk-contact-0.png)

puis aller dans les rules on trouve une règle qui assigne les hosts aux contacts : `Put all hosts into the contact group "all"`

Nous pouvons donc créer des groupes de contactes associé aux équipe et affecter certaine alertes à certaine équipe.

Au niveau utilisateur onb affectera certains utilisateur à certains contact group. 

Exemple : Les alertes mysql mariadb et orcle vers les équipes DBA.

Aussi sur les utilisateur, il est possible de définir ses propre règles d'alertes :

dans le menu utilisateur / bouton notification :

![](../images/cmk-notif-2.png)

Cet envois est très paramétrable, donc chaque utilisateur à la possibilité de paramêtrer finement les alertes qu'il souhaites recevoir et comment il souhaite les recevoirs

> Le point central de checkmk c'est son moteur de règles plutot bien pensé. les alertes ne trouvant pas de destinataire seront alors envoyées à l'adresse mail de fall back (moi j'y mettrais l'adresse du directeur mais il le fait lui même).

> Ce moteur note aussi les matches de règle, ainsi, si on retourne dans le menu wato : hosts and service parameters / Parameters for discovered services. on y trouve le bouton : ![](../images/cmk-rules-4.png) qui liste les règles à non utilisée.

### Snmp monitoring

Alors dans une infra il y a aussi et beaucoup du réseaux, et pour le réseaux c'est en général le protocole snmp qui permet d'avoir une visu global sur l'infra réseaux.

Simple Network Management Protocole, vous connaissez ?

je vous propose de l'activer sous linux pour tester : 

On install et configure snmpd avec vagrant : 
```bash
adminhost$ vagrant provision --provision-with initsnmp lb1 lb2
==> lb1: Running provisioner: initsnmp (shell)...
    lb1: Running: inline script
    lb1: Created symlink from /etc/systemd/system/multi-user.target.wants/snmpd.service to /usr/lib/systemd/system/snmpd.service.
==> lb2: Running provisioner: initsnmp (shell)...
    lb2: Running: inline script
    lb2: Created symlink from /etc/systemd/system/multi-user.target.wants/snmpd.service to /usr/lib/systemd/system/snmpd.service.
```

puis coté interface, menu wato hosts : ![](../images/cmk-snmp-1.png)

On edite les deux hosts lb1 et lb2 un par un (le crayon)
![](../images/cmk-snmp-0.png)

Pour activer snmp pour ces hosts (communauté : 'public').
![](../images/cmk-snmp-2.png)

> Cela serais automatique via la découverte des services au moment de la déclaration du hosts si la communauté par défaut est configuré :
> 
> - dans la gestion central des règle : `Host & Service Parameters`
> - Lancez une recherche sur snmp
>- Ajouter une règle définissant la comunauté snmp par défaut (public) : `SNMP credentials of monitored hosts`

Nous pouvons maintenant relancer la découverte des services

(le crayon sur une feuille)
![](../images/cmk-snmp-0.png)

On lance un full scan, on ajout ou on désactive les sondes qui nous intéresse. Pour ma part j'ai juste gardé le `snmp info` pour la forme mais bon on a déja tout via check_mk.

### Monitoring spécifique asynchrone

J'ai ajouté à la config keepalived un script de notification notification :

```bash
grep -n notify proxy/keepalived.conf1
36:    notify /usr/local/sbin/keepalived-notify.sh
```

voici le script :
```bash
$ cat /usr/local/sbin/keepalived-notify.sh 
#!/bin/bash
TYPE=$1
NAME=$2
STATE=$3
case $STATE in
        "MASTER") echo "0 Vip-$TYPE-$NAME - OK: $STATE" > /var/lib/check_mk_agent/spool/Vip-$TYPE-$NAME.out
                  ;;
        "BACKUP") echo "1 Vip-$TYPE-$NAME - Warning: $STATE" > /var/lib/check_mk_agent/spool/Vip-$TYPE-$NAME.out
                  ;;
        "FAULT")  echo "2 Vip-$TYPE-$NAME - Critical: $STATE" > /var/lib/check_mk_agent/spool/Vip-$TYPE-$NAME.out
                  exit 0
                  ;;
        *)        /sbin/logger "Vip unknown state"
                  exit 1
                  ;;
esac
```

En fait le dossier `/var/lib/check_mk_agent/spool/` contiens des fichiers status check_mk. Ces fichier sont ajouté à la sortie de l'agent, et donc vue par le monitoring comme un service supplémentaire il prend le même format que les script plugins synchrone.

On appele ça asynchrone car en fait les mesures sont faites de façon autonome. Lorsque que l'agent est intérrogé, il donne l'état à la dernière exécution de monitor asynchrone, et ce, même si c'était il y a deux ans.

> A partir de là on peu tout monitorer et notament au niveau métier.

### c'est pas fini

Bon en fait pour ce TP si, mais pas pour faire le tour de check_mk.

A voir : 
- la console d'évènement et le monitoring passif [doc event console](https://checkmk.com/cms_ec.html)
- la gestion des dossier de config/ pour les hosts les services et les règles de configuration
- l'interface c'est sympa mais on a des [commandes](https://checkmk.com/cms_cmk_commandline.html) aussi
- les monitoring tcp (ha bah oui j'ai oublié)
- la surveillance des processus
...
